﻿namespace ado.net_lesson6
{
    public class Person : Entity
    {

        public string FullName { get; set; }
        public int Age { get; set; }
    }
}