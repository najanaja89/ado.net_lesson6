﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson6
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new UsersContext())
            {
                var person = new Person
                {
                    FullName = "John Wick",
                    Age = 50
                };

                var user = new User
                {
                    Login="admin",
                    Password="admin",
                    PersonId=person.Id
                };

                context.People.Add(person);
                context.Users.Add(user);
                //context.SaveChanges();

               var people =  context.People.ToList();
            }
        }
    }
}
