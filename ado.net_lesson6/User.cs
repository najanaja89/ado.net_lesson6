﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson6
{
    public class User : Entity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public Person Person { get; set; }
        public Guid PersonId { get; set; }
    }
}
